/**
 * @module examples.synthvectors
 */
const exports = {};
import olLayerVector from 'ol/layer/Vector.js';
import olSourceVector from 'ol/source/Vector.js';
import olStyleFill from 'ol/style/Fill.js';
import olStyleCircle from 'ol/style/Circle.js';
import olStyleStyle from 'ol/style/Style.js';
import OLCesium from 'olcs/OLCesium.js';
import olView from 'ol/View.js';
import olMap from 'ol/Map.js';
import olSourceOSM from 'ol/source/OSM.js';
import olLayerTile from 'ol/layer/Tile.js';
import olFeature from 'ol/Feature.js';
import olGeomPoint from 'ol/geom/Point.js';
import {defaults as olControlDefaults} from 'ol/control.js';
import olFormatGeoJSON from 'ol/format/GeoJSON.js';
import {transform} from 'ol/proj.js';
import olcsContribManager from 'olcs/contrib/Manager.js';
import {AtlasManager, Circle as CircleStyle, Fill, RegularShape, Stroke, Style} from 'ol/style.js';
import olcsVectorSynchronizer from 'olcs/VectorSynchronizer.js';



let total = 0;
let created = 0;
let added = 0;
var radioValue;
var play = false;

var atlasManager = new AtlasManager({
        // we increase the initial size so that all symbols fit into
        // a single atlas image
        initialSize: 512
})

function getCheckedBoxes(chkboxName) {
  var checkboxes = document.getElementsByName(chkboxName);
  var checkboxesChecked = [];
  // loop over them all
  for (var i=0; i<checkboxes.length; i++) {
     // And stick the checked ones onto an array...
     if (checkboxes[i].checked) {
        checkboxesChecked.push(checkboxes[i].value);
     }
  }
  // Return the array if it is non-empty, or null
  return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

const tile = new olLayerTile({
  source: new olSourceOSM()
});

const map = new olMap({
  layers: [
    new olLayerTile({
      source: new olSourceOSM()
    })
  ],
  target: 'map',
  controls: olControlDefaults({
    attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
      collapsible: false
    })
  }),
  view: new olView({
    center: [0,0],
    zoom: 13
  }),
  renderer: 'webgl'
});
map.getView().setCenter(transform([129.334244, 35.476974],'EPSG:4326','EPSG:3857'));

Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIyMTdmZDFhNy1kOGI4LTQ0ZTUtYjYxNy01ZWU5M2E3MDAzNDgiLCJpZCI6NDM4OCwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU0MDY2ODU1MX0.Q5zq7L4PcMAQALr967ODmvr2WFUq8MQs2NzLUqwxglE';

const ol3d = new OLCesium({
  map: map
});
console.log(ol3d.synchronizer);
const scene = ol3d.getCesiumScene();
scene.terrainProvider = Cesium.createWorldTerrain();
document.getElementById('enable').addEventListener('click', () => ol3d.setEnabled(!ol3d.getEnabled()));
console.log(ol3d.createSynchronizers);
var checkedBoxes = getCheckedBoxes("checkboxHeights");

const odor_style = function getStyle(feature) {
  var odor_color = [];
    odor_color["1000"] = [102,0,204,0]
    odor_color["500_1000"] = [255,0,0,0]
    odor_color["300_500"] = [240,137,24,0]
    odor_color["100_300"] = [255,255,0,0]
    odor_color["50_100"] = [146,208,80,0]
    odor_color["30_50"] = [0,176,80,0]
    odor_color["10_30"] = [43,156,249,0]
    odor_color["5_10"] = [16,78,202,0]
    odor_color["0_5"] = [0,0,153,0]
  var fill;
  checkedBoxes = getCheckedBoxes("checkboxHeights");
  if(feature.values_.value >= 1000){
    fill = new olStyleFill({color: odor_color["1000"]})
  }else if( feature.values_.value >= 500 && feature.values_.value < 1000){
    fill = new olStyleFill({color: odor_color["1000"]})
  }else if(feature.values_.value >= 300 && feature.values_.value < 500){
    fill = new olStyleFill({color: odor_color["300_500"]})
  }else if(feature.values_.value >= 30 && feature.values_.value < 50 ){
    fill = new olStyleFill({color: odor_color["300_500"]})
  }else if(feature.values_.value >= 10 && feature.values_.value < 30 ){
    fill = new olStyleFill({color: odor_color["10_30"]})
  }else if(feature.values_.value >= 5 && feature.values_.value < 10 ){
    fill = new olStyleFill({color: odor_color["5_10"]})
  }else {
    fill = new olStyleFill({color: odor_color["0_5"]})
  }

  if(checkedBoxes.includes(String(feature.values_.geometry.flatCoordinates[2]))){
    fill.color_[3] = 1;
  }
  var style = new olStyleStyle({
        image: new olStyleCircle({
          radius: 2,
          fill: fill,
          // atlasManager: atlasManager,
          opacity:0
        })
      });
      return style;
    }


var source = new olSourceVector();
var minutes = 5;
var emissions = ['so2','tr1','tr2','tr3','tr4']
var height = ['10','30','60','115','225','450','800','1250','1750','2250']
// 20181012224_so2_1250.geojson


// var format = new olFormatGeoJSON();
// var source1 = new olSourceVector({
//       format: new olFormatGeoJSON(),
//       url: 'data/geojson/points.geojson'
//   });


  // var vectorLayers = [vector1,vector2]
// var i = 0;
function changeLayer(vectorLayerSize,queriedHourMinutesth) {
  //data/geojson/20181012_221_so2_10.geojson




}
//
//
// var playLayer = setInterval(changeLayer ,1000);
//
//
//  map.addLayer(vectorLayers[i+1]);
//  map.removeLayer(vectorLayers[i+1]);

 // map.removeLayer(vectorLayers[i])
 // var play = setInterval(function() {
 //
 //   if(i >= vectorLayers.length){
 //    map.removeLayer(vectorLayers[i - 1])
 //    i = 0;
 //   }
 //
 //   if(i == 0){
 //     map.addLayer(vectorLayers[i]);
 //   }else {
 //     map.removeLayer(vectorLayers[i - 1])
 //     map.addLayer(vectorLayers[i]);
 //   }
 //   i++;
 //
 //
 // }, 2000);


 // map.addLayer(vector2);

 // for (var i = 0; i < minutes; i++) {
 //       for (var j = 0; j < height.length; j++) {
 //
 //           var vectorSource = new olSourceVector({
 //                 format: new olFormatGeoJSON(),
 //                 url: 'data/geojson/20181012'+'_220'+i+'_so2_'+height[j]+'.geojson'
 //             });
 //             var vectorLayer = new olLayerVector({
 //               style: odor_style,
 //               source : vectorSource
 //             });
 //             map.addLayer(vectorLayer);
 //       }
 //
 //
 // }



window['handleClick'] = function(myRadio) {
      if(map.getLayers().getLength() > 0){
        map.getLayers().forEach(function(layer){
            if(layer.type == "VECTOR"){
              var features = layer.getSource().getFeatures();
              features.forEach((feature) => {
                  layer.getSource().removeFeature(feature);
              });

            }

        })
      }
      radioValue = myRadio.value * 1;

      for (var i = 0; i < height.length; i++) {

          var vectorSource = new olSourceVector({
                format: new olFormatGeoJSON(),
                url: 'data/geojson/20181012'+'_220_'+emissions[radioValue]+'_'+height[i]+'.geojson'
            });
            var vectorLayer = new olLayerVector({
              style: odor_style,
              source : vectorSource
            });
            map.addLayer(vectorLayer);
      }

  // var checkBox = document.getElementById("checkboxSo2").checked;
};


window['onChecked'] = function() {
  map.getLayers().forEach(function(layer){
      if(layer.type == "VECTOR"){
          var source = layer.getSource();
          var features = source.getFeatures();
          features.forEach(function(feature){
             feature.setStyle(odor_style);
          });

      }

  });
};
// window['playOdor'] = function() {
//     if(map.getLayers().getLength() > 0){
//       map.getLayers().forEach(function(layer){
//           if(layer.type == "VECTOR"){
//             var features = layer.getSource().getFeatures();
//             features.forEach((feature) => {
//                 layer.getSource().removeFeature(feature);
//             });
//
//           }
//
//       })
//     }
//   var vectorSource = new olSourceVector({
//         format: new olFormatGeoJSON(),
//         url: 'data/geojson/20181012_220_so2_60.geojson'
//     });
//     var vectorLayer = new olLayerVector({
//       style: odor_style,
//       source : vectorSource
//     });
//     map.addLayer(vectorLayer);
//
// }

window['playOdor'] = function() {
  var vectorLayerSize = 0;
  if(map.getLayers().getLength() > 0){
    map.getLayers().forEach(function(layer){
        if(layer.type == "VECTOR"){
          var features = layer.getSource().getFeatures();
          features.forEach((feature) => {
              layer.getSource().removeFeature(feature);
          });
          vectorLayerSize++;
        }


    })
  }
  var minutes = 5 ;
  for (var i = 0; i < height.length; i++) {

    for (var j = 0; j < minutes; ++j) {

      if(j == 0){
        var vectorSource = new olSourceVector({
              format: new olFormatGeoJSON(),
              url: 'data/geojson/20181012'+'_22'+j+'_'+emissions[radioValue]+'_'+height[i]+'.geojson'
          });
          var vectorLayer = new olLayerVector({
            style: odor_style,
            source : vectorSource
          });
          map.addLayer(vectorLayer);
      }else{

         var vectorSource = new olSourceVector({
              format: new olFormatGeoJSON(),
              url: 'data/geojson/20181012'+'_22'+j+'_'+emissions[radioValue]+'_'+height[i]+'.geojson'
          });
          var vectorLayer = new olLayerVector({
            style: odor_style,
            source : vectorSource
          });
          vectorLayer.setVisible(false);
          map.addLayer(vectorLayer);

      }
    }
  }

  var timeIndex = 0;
  var queriedHourMinutes = ["220","221","222","223","224"]
  var layers = map.getLayers();
  var playLayer = setInterval(function(){

    layers.forEach(function(layer){
      if(layer.type == "VECTOR"){
        var splitted = layer.values_.source.url_.split(/[/]|[_]|[.]/);
        var hourminute = splitted[3];
        var source = layer.getSource();
        var features = source.getFeatures();
        if( queriedHourMinutes[timeIndex] == hourminute){
          layer.setVisible(true)
        }else{
          layer.setVisible(false)
        }
      }
      timeIndex++;
      // var vectorSynchronizer = new olcsVectorSynchronizer(map,scene);
      // vectorSynchronizer.synchronize();
      if(timeIndex == queriedHourMinutes.length-1){
        timeIndex = 0;
      }
    })
  } ,2000);





};
handleClick(document.emissionForm.myRadios[0]);

export default exports;
